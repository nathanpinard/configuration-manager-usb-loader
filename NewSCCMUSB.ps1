﻿# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}
Set-Variable -Name "currentuser" -Value $env:username
Set-Variable -Name "currentcomputer" -Value $env:computername
Set-Variable -Name "currentdomain" -Value $env:userdomain
write-host " "
write-host " "
write-host " "
write-output "**********SCCM Configuration USB Provisioner 0.1**********"
write-output "Hello $currentuser"
write-output "You are on the domain: $currentdomain"
write-output "The hostname of this machine is: $currentcomputer"
write-host " "
write-output "The following file systems are present:"
get-disk
write-output " "
$Driveletter = read-host "Please select a drive NUMBER to provision. `nWARNING: ONLY SELECT THE USB DRIVE YOU WISH TO FORMAT! `nALL DATA WILL BE ERASED FROM THAT DRIVE!" 
write-output "***********************"
read-host "PRESS ANY KEY TO FORMAT DISK $driveletter"
# Formatting

write-output "Now formatting disk $driveletter ..." 
Clear-Disk -Number $driveletter -RemoveData -confirm:$false | # Out-Null
Initialize-Disk -Number $driveletter -Confirm:$false -PartitionStyle MBR | # Out-Null
write-output "Partitioning disk ..."
New-partition -DiskNumber $driveletter -UseMaximumSize -DriveLetter G -IsActive| Format-Volume -FileSystem FAT32 -NewFileSystemLabel SCCMConfiguration | # Out-Null
write-host "Copying files..."
Copy-Item -Path "C:\sccm_boot\*" -Destination "G:\" -recurse | Out-Null
